# This file is part of fm-actor, a library for interacting with fm-data files:
# https://gitlab.com/sosy-lab/software/fm-actor
#
# SPDX-FileCopyrightText: 2024 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: MIT

from .exceptions import FmDataException  # noqa: F401
from .fmdata import FmData  # noqa: F401

__version__ = "0.3.6.dev0"
