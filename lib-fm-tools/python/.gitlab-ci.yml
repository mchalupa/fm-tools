# This file is part of fm-actor, a library for interacting with fm-data files:
# https://gitlab.com/sosy-lab/software/fm-actor
#
# SPDX-FileCopyrightText: 2024 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: MIT

stages:
  - format
  - checks
  - lib-test
  - lib-build
  - lib-deploy

.py-fm-tools-reuse:
  image:
    name: fsfe/reuse:3
    entrypoint: [""]
  needs: []
  stage: checks
  script:
    - cd lib-fm-tools/python
    - reuse lint

py-fm-tools-lint:
  image: python:3.12-slim
  stage: checks
  needs: []
  before_script:
    - python -m pip install ruff
  script:
    - cd lib-fm-tools/python
    - ruff check

py-fm-tools-test:
  image: python:3.12-slim
  stage: lib-test
  needs:
    - job: "py-fm-tools-lint"
  before_script:
    - python -m pip install hatch
  script:
    - cd lib-fm-tools/python
    - python -m hatch run test:test
  only:
    changes:
      - "lib-fm-tools/python/**/*"

py-fm-tools-build:
  image: python:3.12-slim
  needs:
    - job: "py-fm-tools-test"
  stage: lib-build
  before_script:
    - python -m pip install hatch
  script:
    - cd lib-fm-tools/python
    - python -m hatch build
  artifacts:
    paths:
      - dist/*.whl
  only:
    changes:
      - "lib-fm-tools/python/**/*"

py-fm-tools-pypi:
  image: python:3.12-slim
  stage: lib-deploy
  needs:
    - job: "py-fm-tools-build"
      artifacts: true
  script:
    - cd lib-fm-tools/python
    - pip install -U twine
    - twine upload -u "__token__" -p "$TWINE_TOKEN" dist/*
  rules:
    - if: '$CI_COMMIT_TAG =~ /^lib-py-\d{1,2}\.\d{1,2}\.[^ ]*$/'
      # Always publish tagged commits closely matching Semantic Versioning
      when: on_success
