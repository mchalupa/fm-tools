# Collection of Information about Formal-Methods Tools

## Motivation

There are many tools available that implement formal-methods approaches.
This repository collects meta data about the tools, such that
it becomes easier to reuse, integrate, and cooperate with formal-methods tools.

A [description](https://www.sosy-lab.org/research/pub/2024-Podelski65.Find_Use_and_Conserve_Tools_for_Formal_Methods.pdf)
of the structure of this repository can be found in an article.

A [formatted listing](https://fm-tools.sosy-lab.org/)
of some of the data in this repository can be found on a generated web site.

A [schema definition](https://fm-tools.sosy-lab.org/schema.html)
of the data files in this repository can be found on a generated web site.
