$schema: "https://json-schema.org/draft/2020-12/schema"
title: FM Tools Data Schema
description: The schema for fm-tools/data
type: object
properties:
  name:
    type: string
    description: The name of the competition tool.
  description:
    type: string
    description: A description of the tool.
  input_languages:
    type: array
    items:
      type: string
    description: The supported input languages of the tool.
  project_url:
    type: string
    format: uri
    description: The URL of the project associated with the tool.
  repository_url:
    type: string
    format: uri
    description: The URL of the tool's repository.
  spdx_license_identifier:
    type: string
    description: The SPDX license identifier of the tool.
  benchexec_toolinfo_module:
    type: string
    description: Tool-info module name of benchexec in the format benchexec.tools.<tool>.
  fmtools_format_version:
    type: string
    description: The version of the fm-tools/data format.
  fmtools_entry_maintainers:
    type: array
    items:
      type: string
    description: Maintainers of the tool.
  maintainers:
    type: array
    items:
      type: object
      properties:
        orcid:
          type: string
          pattern: '^[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{3}[0-9X]$'
        name:
          type: string
        institution:
          type: string
        country:
          type: string
        url:
          type:
            - string
            - "null"
          format: uri
      required:
        - orcid
        - name
        - institution
        - country
        - url
    description: Information about the maintainers of the tool.
  versions:
    type: array
    items:
      type: object
      properties:
        version:
          type: string
        doi:
          type: [string, "null"]
          pattern: '10\.5281/zenodo\.[0-9]+$'
        url:
          type:
            - string
            - "null"
          format: uri
        benchexec_toolinfo_options:
          type: array
          items:
            type: string
        required_ubuntu_packages:
          type: array
          items:
            type: string
      oneOf:
        - required:
            - version
            - benchexec_toolinfo_options
            - required_ubuntu_packages
            - doi
        - required:
            - version
            - benchexec_toolinfo_options
            - required_ubuntu_packages
            - url
    description: Information about different versions of the tool.
  competition_participations:
    type: array
    items:
      type: object
      properties:
        competition:
          type: string
          pattern: "(SV-COMP|Test-Comp) 20[1-9][0-9]$"
        track:
          type: string
          enum:
            - Validation of Correctness Witnesses 1.0
            - Validation of Correctness Witnesses 2.0
            - Validation of Violation Witnesses 1.0
            - Validation of Violation Witnesses 2.0
            - Verification
            - Test Generation
            - Validation of Test Suites
        tool_version:
          type: string
        jury_member:
          type: object
          properties:
            orcid:
              type: string
              pattern: '^[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{3}[0-9X]$'
            name:
              type: string
            institution:
              type: string
            country:
              type: string
            url:
              type:
                - string
                - "null"
          required:
            - name
            - institution
            - country
            - url
      required:
        - competition
        - track
        - tool_version
        - jury_member
    description: Information about the tool's participation in competitions.
  techniques:
    type: array
    items:
      type: string
      oneOf:
        - title: CEGAR
          const: CEGAR
          description: |
            CounterExample-Guided Abstraction Refinement is a model-checking technique
            that iteratively refines the abstract model of the transition system by
            analyzing spurious counterexamples and learning a more precise abstraction.

            Literature:

            - ['Counterexample-Guided Abstraction Refinement', Proc. CAV, 2000](https://doi.org/10.1007/10722167_15)
            - ['Boolean Programs: A Model and Process for Software Analysis', TR 2000-14, 2000](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/tr-2000-14.pdf)
            - ['Counterexample-Guided Abstraction Refinement for Symbolic Model Checking', J. ACM, 2003](https://doi.org/10.1145/876638.876643)

        - title: Predicate Abstraction
          const: Predicate Abstraction
          description: |
            Predicate Abstraction is an abstract domain
            in which abstract states represent a set of concrete states using predicated from a given set of predicates (called the precision).
            The set of predicates is fixed for one abstraction computation, but can chance from location to location.
            There are two kinds of predicate abstraction: cartesian predicate abstraction and boolean predicate abstraction.

            Literature:

            - ['Construction of Abstract State Graphs with PVS', Proc. CAV, 1997](https://doi.org/10.1007/3-540-63166-6_10)
            - ['Boolean and Cartesian Abstraction for Model Checking C Programs', Proc. TACAS, 2001](https://doi.org/10.1007/3-540-45319-9_19)

        - title: Symbolic Execution
          const: Symbolic Execution
          description: |
            Symbolic Execution is model-checking technique
            in which a symbolic store assigns values to variables,
            where the symbolic values can be constrained by a set of path constraints.

            Literature:

            - ['Symbolic Execution and Program Testing', Comm. ACM, 1976](https://doi.org/10.1145/360248.360252)

        - title: Bounded Model Checking
          const: Bounded Model Checking
          description: |
            Bounded Model Checking (BMC) is a model-checking technique
            that unrolls the transition system a finite number of times.

            Literature:

            - ['Symbolic Model Checking without BDDs', Proc. TACAS, 1999](https://doi.org/10.1007/3-540-49059-0_14)
            - ['Bounded Model Checking', Adv. Comput., 2003](https://doi.org/10.1016/S0065-2458(03)58003-2)

        - title: k-Induction
          const: k-Induction
          description: |
            k-Induction generalizes 1-induction.
            1-Induction uses as premise that the property holds for the (one) previous object, while
            k-induction generalizes it to using as premise that the property holds for the k previous objects.

            Literature:

            - ['The k-Induction Principle', Online, 2013](http://www.ccs.neu.edu/home/wahl/Publications/k-induction.pdf)
            - ['PKind: A Parallel k-Induction Based Model Checker', EPTCS, 2011](https://doi.org/10.4204/EPTCS.72.6)

        - title: Property-Directed Reachability
          const: Property-Directed Reachability
          description: |
            Property-Directed Reachability is a model-checking technique
            that constructs a sequence of invariants (also called frames)
            that describe the reachable states after a number of transitions,
            by continuously refining the invariants directed by paths that
            reach a target state.

            Literature:

            - ['SAT-Based Model Checking Without Unrolling', Proc. VMCAI, 2011](https://doi.org/10.1007/978-3-642-18275-4_7)
            - ['Property-Directed Incremental Invariant Generation', Formal Asp. Comput., 2008](https://doi.org/10.1007/s00165-008-0080-9)

        - title: Explicit-Value Analysis
          const: Explicit-Value Analysis
          description: |
            Explicit-Value Analysis is a model-checking technique
            that stores explicit values for program variables,
            with similarities to constant-propagation data-flow analyses
            and explicit-state model checking.

            Literature:

            - ['Explicit-State Model Checking', Handbook of Model Checking, 2018](https://doi.org/10.1007/978-3-319-10575-8_5)
            - ['A Unified Approach to Global Program Optimization', Proc. POPL, 1973](https://doi.org/10.1145/512927.512945)
            - ['Explicit-State Software Model Checking Based on CEGAR and Interpolation', Proc. FASE, 2013](https://doi.org/10.1007/978-3-642-37057-1_11)

        - title: Numeric Interval Analysis
          const: Numeric Interval Analysis
          description: |
            Numeric Interval Analysis is a data-flow analysis
            that is based on the abstract domain of intervals,
            in which each program variable is mapped to an interval of values.

            Literature:

            - ['Compiler Analysis of the Value Ranges for Variables', IEEE Trans. Software Eng., 1977](https://doi.org/10.1109/TSE.1977.231133)
            - ['Abstract Interpretation: A Unified Lattice Model for Static Analysis of Programs by Construction or Approximation of Fixpoints', Proc. POPL, 1977](https://doi.org/10.1145/512950.512973)

        - title: Shape Analysis
          const: Shape Analysis
          description: |
            Shape Analysis

        - title: Separation Logic
          const: Separation Logic
          description: |
            Separation Logic provides reasoning about the heap,
            which makes is particularly useful for verifying memory safety
            and other properties that require the analysis of the program heap.

            Literature:

            - ['Separation Logic', Comm. ACM, 2019](https://doi.org/10.1145/3211968)
            - ['An Overview of Separation Logic', Proc. VSTTE, 2008](https://doi.org/10.1007/978-3-540-69149-5_49) 
            - ['Local Reasoning about Programs that Alter Data Structures', Proc. CSL, 2001](https://doi.org/10.1007/3-540-44802-0_1)

        - title: Bit-Precise Analysis
          const: Bit-Precise Analysis
          description: |
            Bit-Precise Analysis is a technique in which the
            abstract domain for tracking values treats variables of type int
            as bit vectors, instead of approximating them as (unbounded) integers.

        - title: ARG-Based Analysis
          const: ARG-Based Analysis
          description: |
            ARG-Based Analysis is a model-checking technique
            that stores successor relations in the set of reached abstract states,
            which is particularly useful for constructing error paths
            once a violation of the specification is found.

            Literature:

            - ['The Software Model Checker Blast: Applications to Software Engineering', Int. J. Softw. Tools Technol. Transf., 2007](https://doi.org/10.1007/s10009-007-0044-z)

        - title: Lazy Abstraction
          const: Lazy Abstraction
          description: |
            Lazy Abstraction is a technique from software model checking
            that lazily refines the abstract model only when and where needed,
            resulting in abstract models (ARGs) that can have different precisions
            on different paths and on different locations on a path.

            Literature:

            - ['Lazy Abstraction', Proc. POPL, 2002](https://doi.org/10.1145/503272.503279)
            - ['The Software Model Checker Blast: Applications to Software Engineering', Int. J. Softw. Tools Technol. Transf., 2007](https://doi.org/10.1007/s10009-007-0044-z)

        - title: Interpolation
          const: Interpolation
          description: |
            Interpolation is a technique from model checking
            that computes for a two formulas A and C, for which A → C holds,
            a new formula B such that A → B → C
            and B contains only symbols that occur in A and C.

            Literature:

            - ['Linear Reasoning. A New Form of the Herbrand-Gentzen Theorem', J. Symb. Log., 1957](https://doi.org/10.2307/2963593)
            - ['Interpolation and SAT-Based Model Checking', Proc. CAV, 2003](https://doi.org/10.1007/978-3-540-45069-6_1)

        - title: Automata-Based Analysis
          const: Automata-Based Analysis
          description: |
            Automata-Based Analysis is a technique from software model checking
            in which automata are used in the analysis algorithm,
            either to represent the abstract state space or the specification.

            Literature:

            - ['Software Model Checking for People Who Love Automata', Proc. CAV, 2013](https://doi.org/10.1007/978-3-642-39799-8_2)
            - ['The BLAST Query Language for Software Verification', Proc. SAS, 2004](https://doi.org/10.1007/978-3-540-27864-1_2)

        - title: Concurrency Support
          const: Concurrency Support
          description: |
            Concurrency Support comprises techniques
            that make it possible to verify concurrent programs,
            which use more than one execution thread and shared memory.

        - title: Ranking Functions
          const: Ranking Functions
          description: |
            Ranking Functions are used to prove the termination of loops.
            A ranking function maps the program's state to a well-founded set,
            often the natural numbers, such that every loop iteration of the program
            strictly decreases the value of the function.
            If a ranking function for a loop can be found,
            it guarantees that the loop terminates.

            Literature:

            - ['A Complete Method for the Synthesis of Linear Ranking Functions', Proc. VMCAI, 2004](https://doi.org/10.1007/978-3-540-24622-0_20)

        - title: Evolutionary Algorithms
          const: Evolutionary Algorithms
          description: |
            Evolutionary Algorithms are heuristic optimization algorithms
            that are inspired by biological evolution.
            They use mechanisms such as mutation, crossover, and selection
            to evolve a population of candidate solutions
            towards an optimal or near-optimal solution.

            Literature:

            - ['Genetic Algorithms', Coll. Search Methodologies, 2005](https://doi.org/10.1007/0-387-28356-0_4)

        - title: Algorithm Selection
          const: Algorithm Selection
          description: |
            Algorithm Selection is a technique that
            inspects the input and based on information from the input
            (for example, by extracting a feature vector) selects one
            algorithm (that seems suited for the input)
            from a set of given algorithms.

            Literature:

            - ['The Algorithm Selection Problem', Adv. Comput., 1976](https://doi.org/10.1016/S0065-2458(08)60520-3)

        - title: Portfolio
          const: Portfolio
          description: |
            Portfolio is a technique from economics
            in which the risk is spread over several targets.
            Algorithmic portfolios run several algorithms
            in parallel or in sequence (while sharing resources),
            in the hope that one of the algorithms finds the solution.

            Literature:

            - ['An Economics Approach to Hard Computational Problems', Science, 1997](https://doi.org/10.1126/science.275.5296.51)

        - title: Interpolation-Based Model Checking
          const: Interpolation-Based Model Checking
          description: |
            Interpolation-Based Model Checking is a model-checking algorithm from 2003
            that uses interpolants to over-approximate sets of reachable states.

            Literature:

            - ['Interpolation and SAT-Based Model Checking', Proc. CAV, 2003](https://doi.org/10.1007/978-3-540-45069-6_1)

        - title: Logic Synthesis
          const: Logic Synthesis
          description: |
            Logic Synthesis

            Literature:

            - ['Advanced Logic Synthesis', Springer, 2018](https://doi.org/10.1007/978-3-319-67295-3)

        - title: Task Translation
          const: Task Translation
          description: |
            Task Translation is a technique
            that translates a verification task t into another verification task t'
            such that an existing verification technique for t' can be used to derive
            a verification result for t.

            Literature:

            - ['Testability Transformation', IEEE Trans. Softw. Eng., 2004](https://doi.org/10.1109/TSE.2004.1265732)

        - title: Floating-Point Arithmetics
          const: Floating-Point Arithmetics
          description: |
            Floating-Point Arithmetics provides
            precise reasoning about floating-point numbers in programs.
            For example, expressions in the program can be translated
            to SMT formulas using the theory of floating-point arithmetics.

            Literature:

            - ['Handbook of Floating-Point Arithmetic', Springer, 2018](https://doi.org/10.1007/978-3-319-76526-6)
            - ['What Every Computer Scientist Should Know About Floating-Point Arithmetic', ACM Comput. Surv., 1991](https://doi.org/10.1145/103162.103163)

        - title: Guidance by Coverage Measures
          const: Guidance by Coverage Measures
          description: |
            Guidance by Coverage Measures is a technique
            that uses coverage measures to guide the process of verification or test generation.

        - title: Random Execution
          const: Random Execution
          description: |
            Random Execution is a technique for black-box testing
            in which input values are randomly generated to construct
            a test suite for the system under test.

        - title: Targeted Input Generation
          const: Targeted Input Generation
          description: |
            Targeted Input Generation is a technique for software testing
            that focuses on generating input values
            that are likely to trigger specific behaviors or execution paths in the system.

    description: Techniques used by the tool.
  frameworks_solvers:
    type: array
    items:
      type: string
      oneOf:
        - title: CPAchecker
          const: CPAchecker
          description: |
            CPAchecker

        - title: CProver
          const: CProver
          description: |
            CProver

        - title: ESBMC
          const: ESBMC
          description: |
            ESBMC

        - title: JPF
          const: JPF
          description: |
            JPF

        - title: Ultimate
          const: Ultimate
          description: |
            Ultimate

        - title: JavaSMT
          const: JavaSMT
          description: |
            JavaSMT

        - title: MathSAT
          const: MathSAT
          description: |
            MathSAT

        - title: CVC
          const: CVC
          description: |
            CVC

        - title: SMTinterpol
          const: SMTinterpol
          description: |
            SMTinterpol

        - title: Z3
          const: Z3
          description: |
            Z3

        - title: MiniSAT
          const: MiniSAT
          description: |
            MiniSAT

        - title: Apron
          const: Apron
          description: |
            Apron

    description: Frameworks or solvers used by the tool.
  used_actors:
    type: array
    items:
      type: object
      properties:
        actor_type:
          type: string
        description:
          type: string
      required:
          - actor_type
          - description
    description: Actors/components that are used by the tool.
  literature:
    type: array
    items:
      type: object
      properties:
        doi:
          type: string
          pattern: '10\.[0-9]{4,9}\/.*'
        title:
          type: string
        year:
          type: integer
      required:
        - doi
        - title
        - year
    description: Relevant literature references for the tool.
  licenses:
    type: array
    items:
      type: object
      properties:
        name: 
          type: string
        body:
          type: string
      required:
        - name
        - body
    description: Custom licenses used by the tool (applicable if no matching SPDX identifier exists).
required:
  - name
  - input_languages
  - project_url
  - spdx_license_identifier
  - benchexec_toolinfo_module
  - fmtools_format_version
  - fmtools_entry_maintainers
  - maintainers
  - versions
  - competition_participations
  - techniques
  - frameworks_solvers
  - literature
